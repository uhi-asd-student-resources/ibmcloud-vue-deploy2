# ibmcloud-vue-deploy

## Creating a new BitBucket repo with a Vue project

1. Create Vue project locally using ```vue create``` command
2. Create an **empty** repository without a gitignore or Readme
3. In the vue project directory type:
   
```
git remote add origin <bitbucket git repository url e.g. git@bitbucket.org/user/project.git>
git push -u origin master
```

Note that this works because Vue automatically commits your files to a git repository.  If this was a directory which was not a git repository you would need to do the following:

```
git init
git add --all
git commit -m "Initial commit"
```

Reference: https://confluence.atlassian.com/bitbucketserver/importing-code-from-an-existing-project-776640909.html

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
